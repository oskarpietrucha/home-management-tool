import app from 'firebase/app';
import 'firebase/auth';
import 'firebase/database';

const firebaseConfig = {
  apiKey: "AIzaSyBOVA7i1T6tLCeVRY3q7coK6IoBZQ8FpWk",
  authDomain: "home-management-tool.firebaseapp.com",
  databaseURL: "https://home-management-tool.firebaseio.com",
  projectId: "home-management-tool",
  storageBucket: "home-management-tool.appspot.com",
  messagingSenderId: "663097665518",
  appId: "1:663097665518:web:e560f578de266c4b6390ff",
  measurementId: "G-DQJJ7N3N1M"
};
class Firebase {
  constructor() {
    app.initializeApp(firebaseConfig);

    this.auth = app.auth();
    this.db = app.database();
  }

  // *** Auth API ***
  doCreateUserWithEmailAndPassword = (email, password) => 
    this.auth.createUserWithEmailAndPassword(email, password);

  doSignInWithEmailAndPassword = (email, password) =>
    this.auth.signInWithEmailAndPassword(email, password);

  doSignOut = () => this.auth.signOut();

  doPasswordReset = email => this.auth.sendPasswordResetEmail(email);

  doPasswordUpdate = password => 
    this.auth.currentUser.updatePassword(password);

  // *** User API ***

  user = uid => this.db.ref(`users/${uid}`);

  users = () => this.db.ref('users');
}

export default Firebase;